from setuptools import setup

setup(
    name='dhutilities',
    version='0.0.1',
    description='My private package from private github repo',
    url='https://runepoulsen@bitbucket.org/runepoulsen/dhutilities.git',
    author='Rune',
    author_email='runesp@gmail.com',
    license='unlicense',
    packages=['dhutilities'],
    install_requires=['PyYAML', 'pyyaml-include', 'Pillow'],
    zip_safe=False
)
