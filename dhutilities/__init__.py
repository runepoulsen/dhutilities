import json
from importlib.abc import Loader
from os import path
from pathlib import Path

from PIL import Image
from yamlinclude import YamlIncludeConstructor
import io
import yaml

YamlIncludeConstructor.add_to_loader_class(loader_class=yaml.FullLoader)


def get_image_bytes(image, quality=90):
    image = Image.fromarray(image)
    with io.BytesIO() as bytesIO:
        image.save(bytesIO, "JPEG", quality=quality, optimize=True)
        return bytesIO.getvalue()


def load_config(filename, directory="config", file_ext='yaml'):
    config = {}
    Path(directory).mkdir(exist_ok=True)
    file = '%s.%s' % (filename, file_ext)
    file_path = path.join(*[directory, file])
    if path.exists(file_path):
        with open(file_path) as data:
            if file_ext is 'json': config = json.load(data)
            if file_ext is 'yaml' or file_ext is 'yml': config = yaml.load(data, Loader=yaml.FullLoader)
            # if file_ext is 'yaml' or file_ext is 'yml': config = yaml.safe_load(data)
    return config


def save_config(filename, data, directory="config", file_ext="yaml"):
    Path(directory).mkdir(exist_ok=True)
    file = '%s.%s' % (filename, file_ext)
    file_path = path.join(*[directory, file])
    with open(file_path, "w") as file:
        yaml.dump(data, file)
